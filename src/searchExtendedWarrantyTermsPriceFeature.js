import {inject} from 'aurelia-dependency-injection';
import TermsPriceServiceSdkConfig from './termsPriceServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import ExtendedWarrantyRequestRuleWebView from './extendedWarrantyRequestRuleWebView';
import ExtendedWarrantyResponseRuleWebView from './extendedWarrantyResponseRuleWebView';

@inject(TermsPriceServiceSdkConfig, HttpClient)
class SearchExtendedWarrantyTermsPriceFeature {

    _config:TermsPriceServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:TermsPriceServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }


    /**
     * @param {ExtendedWarrantyRequestRuleWebView} request
     * @param {string} accessToken
     * @returns {Promise.<ExtendedWarrantyResponseRuleWebView>}
     */
    execute(request:ExtendedWarrantyRequestRuleWebView,
            accessToken:string):Promise<ExtendedWarrantyResponseRuleWebView> {

        return this._httpClient
            .createRequest('extendedWarranty-terms-price/searchExtendedWarrantyTermsPrice')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);

    }

}
export default SearchExtendedWarrantyTermsPriceFeature;