import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import TermsPriceServiceSdkConfig from './termsPriceServiceSdkConfig';
import ExtendedWarrantyTermsFeature from './extendedWarrantyTermsFeature';
import SearchExtendedWarrantyTermsPriceFeature from './searchExtendedWarrantyTermsPriceFeature';

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {TermsPriceServiceSdkConfig} config
     */
    constructor(config:TermsPriceServiceSdkConfig){

        if(!config){
            throw 'config required';
        }
        this._container = new Container();

        this._container.registerInstance(TermsPriceServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);
        this._registerFeatures();
    }

    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures(){
        this._container.autoRegister(ExtendedWarrantyTermsFeature);
        this._container.autoRegister(SearchExtendedWarrantyTermsPriceFeature);
    }

}