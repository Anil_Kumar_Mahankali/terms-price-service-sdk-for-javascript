/**
 * @class {TermsPriceServiceSdkConfig}
 */
export default class TermsPriceServiceSdkConfig {

    _precorConnectApiBaseUrl: string;

    constructor(precorConnectApiBaseUrl:string){

        if(!precorConnectApiBaseUrl) {
            throw 'precorConnectBaseUrl required';
        }
        this._precorConnectApiBaseUrl = precorConnectApiBaseUrl;
    }

    /**
     * @returns string
     */

    get precorConnectApiBaseUrl():string {
        return this._precorConnectApiBaseUrl;
    }

}