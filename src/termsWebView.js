/**
 * @class {TermsWebView}
 */
export default class TermsWebView {

    _term: string;

    /**
     * @param term
     */
    constructor(
        term:string
    ) {
        if(!term){
            throw new TypeError('term required');
        }
        this._term = term;
    }

    /**
     * getter methods
     */
    get term():string{
        return this._term;
    }

    toJSON() {
        return {
            term:this._term
        }
    }
}