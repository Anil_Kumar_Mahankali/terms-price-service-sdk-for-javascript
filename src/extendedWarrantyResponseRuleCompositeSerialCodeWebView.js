import ExtendedWarrantyResponseRuleSimpleSerialCodeWebView from './extendedWarrantyResponseRuleSimpleSerialCodeWebView';
/**
 * @class {ExtendedWarrantyResponseRuleCompositeSerialCodeWebView}
 */
export default class ExtendedWarrantyResponseRuleCompositeSerialCodeWebView {

    _components:ExtendedWarrantyResponseRuleSimpleSerialCodeWebView[];

    _term:string;

    _price:number;

    _materialNumber:string;

    _isEWeligible:boolean;


    /**
     * @param {ExtendedWarrantyResponseRuleSimpleSerialCodeWebView} components
     * @param {string} term
     * @param {number} price
     * @param {string} materialNumber
     * @param {boolean} isEWeligible
     */
    constructor(
        components:ExtendedWarrantyResponseRuleSimpleSerialCodeWebView[],
        term:string,
        price:number,
        materialNumber:string,
        isEWeligible:boolean
    ){

        this._components = components;

        this._term = term;

        this._price = price;

        this._materialNumber = materialNumber;

        this._isEWeligible = isEWeligible;

    }

    /**
     * getter methods
     */
    get components():ExtendedWarrantyResponseRuleSimpleSerialCodeWebView[] {
        return this._components;
    }

    get term(): string {
        return this._term;
    }

    get price(): number {
        return this._price;
    }

    get materialNumber(): string {
        return this._materialNumber;
    }

    get isEWeligible(): boolean  {
        return this._isEWeligible;
    }


    toJSON() {
        return {
            components : this._components,
            term : this._term,
            price : this._price,
            materialNumber : this._materialNumber,
            isEWeligible : this._isEWeligible
        }
    }
}
