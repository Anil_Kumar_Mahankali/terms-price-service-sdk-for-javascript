## Description
Precor Connect Terms Price service SDK for javascript.


## Setup

**install via jspm**
```shell
jspm install terms-price-service-sdk=bitbucket:precorconnect/terms-price-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import TermsPriceServiceSdk,{TermsPriceServiceSdkConfig} from 'terms-price-service-sdk'

const termsPriceServiceSdkConfig =
    new TermsPriceServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );

const TermsPriceServiceSdk =
    new TermsPriceServiceSdk(
        termsPriceServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```