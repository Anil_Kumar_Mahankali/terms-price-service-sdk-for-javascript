/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    firstName: 'DummyFirstName',
    lastName: 'DummyLastName',
    partnerAccountId: "123456781234567899",
    url: 'https://dummy-url.com',
    serialNumber:"AJTE",
    term:"3/2"
};



