import TermsPriceServiceSdk,{
    TermsWebView,
    ExtendedWarrantyRequestRuleWebView,
    ExtendedWarrantyResponseRuleWebView
} from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be TermsPriceServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new TermsPriceServiceSdk(config.termsPriceServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(TermsPriceServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('extendedWarrantyTerms method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new TermsPriceServiceSdk(config.termsPriceServiceSdkConfig);

                /*
                 act
                 */
                const termsPricePromise =
                    objectUnderTest
                        .extendedWarrantyTerms(
                            factory
                                .constructValidAppAccessToken()
                        );

                /*
                 assert
                 */
                termsPricePromise
                    .then((TermsWebView) => {
                        expect(TermsWebView.length >= 1).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        describe('searchExtendedWarrantyTermsPrice method', () => {
            it('should return ExtendedWarrantyResponseRuleWebView', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new TermsPriceServiceSdk(config.termsPriceServiceSdkConfig);

                let extendedWarrantyRequestRuleWebView =
                    new ExtendedWarrantyRequestRuleWebView(
                        dummy.term,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .searchExtendedWarrantyTermsPrice(
                            extendedWarrantyRequestRuleWebView,
                            factory.constructValidAppAccessToken()
                        );

                /*
                 assert
                 */
                actPromise
                    .then((ExtendedWarrantyResponseRuleWebView) => {
                        expect(ExtendedWarrantyResponseRuleWebView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });
    });
});